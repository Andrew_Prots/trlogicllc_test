import {setLocal, getLocal} from '../services/ls'

export default {
  addToBasket: (state, id) => {
    let item = state.basket.find((e) => e.id == id)
    if (item) {
      item.count++
    } else {
      state.basket.unshift({
        id: id,
        count: 1
      })
    }
    setLocal({key: 'basket', value: state.basket})
  },
  decreaseFromBasket (state, id) {
    let item = state.basket.find((e) => e.id == id)
    if (item.count > 1) {
      item.count--
      setLocal({key: 'basket', value: state.basket})
    }
  },
  deleteFromBasket (state, id) {
    state.basket.splice(state.basket.findIndex(e => e.id == id), 1)
    setLocal({key: 'basket', value: state.basket})
  },
  clearBasket (state) {
    state.basket = []
    setLocal({key: 'basket', value: state.basket})
  },
  initBasketFromLocal (state) {
    let localBasket = getLocal('basket')
    if (localBasket) {
      state.basket = localBasket
    }
  }
}
