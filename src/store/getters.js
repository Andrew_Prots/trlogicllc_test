export default {
  getItems: state => state.items,
  getBasket: state => state.basket,
  getBasketLength: state => state.basket.reduce((a, b) => a += b.count, 0),
  getBasketItem: state => id => state.items.find((e) => e.id == id),
  getTotalPrice: state => state.basket
    .reduce((a, b) => a += state.items
    .find((e) => e.id == b.id).price * state.basket.find(e => e.id == b.id).count, 0)
}
