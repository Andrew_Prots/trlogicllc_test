export default {
  items: [
    {
      id: 0,
      price: 429,
      name: 'Rust',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/252490/ced8982cc46ce2b31cdb746f0abf61e9e8935913.jpg'
    },
    {
      id: 1,
      price: 474,
      name: 'PLAYERUNKNOWN\'S BATTLEGROUNDS',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/578080/2d2732a33511b58c69aff6b098a22687a3bb8533.jpg'
    },
    {
      id: 2,
      price: 629,
      name: 'Grand Theft Auto V',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/271590/e447e82f8b0c67f9e001498503c62f2a187bc609.jpg'
    },
    {
      id: 3,
      price: 539,
      name: 'The Witcher 3: Wild Hunt',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/292030/2f22c2e5528b78662988dfcb0fc9aad372f01686.jpg'
    },
    {
      id: 4,
      price: 845,
      name: 'Assassin\'s Creed Odyssey',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/812140/fb3d894fb2a929df5fb0417f92864398a64909c6.jpg'
    },
    {
      id: 5,
      price: 845,
      name: 'Assassin\'s Creed Origins',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/582160/ad2aa9eb6848b10ad2ff27d2dd881dc05e2a8356.jpg'
    },
    {
      id: 6,
      price: 350,
      name: 'Factorio',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/427520/95e5aa627ae1481b1dea293f6db5954e8aa79f41.jpg'
    },
    {
      id: 7,
      price: 640,
      name: 'Metro Exodus',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/412020/e44a659648502b1ae5d8cf15f8609d682ff0d5e0.jpg'
    },
    {
      id: 8,
      price: 699,
      name: 'Rise of the Tomb Raider',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/391220/5270d12abfc683e6f1d39e7ac0b6f2db366e7209.jpg'
    },
    {
      id: 9,
      price: 279,
      name: 'Mortal Kombat X',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/307780/80dd0a53aa38fb750b23a3f0c1f346dce7dd464c.jpg'
    },
    {
      id: 10,
      price: 525,
      name: 'Sid Meier\'s Civilization VI',
      img: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/289070/356443a094f8e20ce21293039d7226eac3d3b4d9.jpg'
    }
  ],
  basket: []
}
