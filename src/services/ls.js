const setLocal = ({key, value}) => {
  window.localStorage.setItem(key, JSON.stringify(value))
}

const getLocal = (key) => {
  return JSON.parse(window.localStorage.getItem(key))
}

export {
  setLocal,
  getLocal
}
